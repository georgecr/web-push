# Push Notifications codelab

https://developers.google.com/web/fundamentals/codelabs/push-notifications/

Code for the Web Fundamentals [Push Notifications codelab](https://codelabs.developers.google.com/codelabs/push-notifications/).

In this codelab, you'll learn how to add Push Notifications to web applications. This will enable you to re-engage users with breaking news and information about
new content.

You'll also learn the basics of Service Workers.

## What you'll learn

* Service Worker basics: installation and event handling
* How to set up a Google Cloud Messaging (GCM) account
* How to add a web manifest
* Techniques for requesting GCM to send a notification to a web client
* Notification display
* Notification click handling

Example code for each step of the codelab is available from the [completed](completed/) directory.

## Example push

```bash
  web-push send-notification --endpoint=https://fcm.googleapis.com/fcm/send/eeotSBqDkqQ:APA91bH-UJUvWYuyP4Ggzs9HYuq3u_p_xhhOxoHi2FS8ofxSqU0vZGOr7rd5Vg-V_Dc-2GRny4LW6ByXTNfFUHDrQNIzlZ7fVxnRS3eApYrkHk2y5snATBRfdR1bpJOI3UVme_vgCnnQ --key=BB38M13JaQiZ5WETHfjO-AXY4q5Z-d3ZPVVdjJnqF_G00xo6hLU8tC688J4FK2HUagqyzpDDDrVobeo1e43PZL0 --auth=dsxMpyDgarfP27gJpfLGFA --payload=salmon --vapid-subject=mailto:example@example.com --vapid-pubkey=BOMxKiHqsFFly3BFkb_z7N625pdz5Mz0wRokmol_I-EW4kORiH6Cageeg_xCw--M3F-fB-cauMtRsGzg987BDrk --vapid-pvtkey=FWvDSQnSAb4fxDPlcP0o-4rBAp7Ydfzq7xa6Rfq9cQ8 

```